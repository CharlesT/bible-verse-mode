;;; bible-verse-mode.el --- Mode for inserting bible quotations into org files
;;; -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Charles Taylor
;;
;; Author: Charles Taylor <https://github.com/chuckles>
;; Maintainer: Charles Taylor <taylorc822@gmail.com>
;; Created: August 07, 2021
;; Modified: August 07, 2021
;; Version: 0.0.1
;; Keywords: convenience org bible
;; Homepage: https://github.com/chuckles/bible-verse-mode
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;; I thought it would be useful for me.
;;
;;; Code:


(defcustom bible-verse-contents "both"
  "The expected contents of a bible quotation.

Possible values:
both - insertions include a link and a quotation.
link - insertions include just a link
quote - insertions include just a quotation."
  :type 'string
  :group 'bible-verse
  :options '("both" "link" "quote"))


(defcustom bible-verse-source "http://labs.bible.org/api/?"
  "Source of bible text. TODO In need of extension & reworking.
Some kind of alist would help."
  :type 'string
  :group 'bible-verse
  :options '("http://labs.bible.org/api/?"))


(defcustom bible-verse-translation "NET"
  "Bible translation to request.
Note that valid values of this variable may depend on the value of
'bible-verse-source', since not all translations are available
everywhere. Just be reasonable - e.g. NET is only available at
netbible.com."
  :group 'bible-verse
  :type 'string
  :options '("NET" "NIV" "ESV"))

(defcustom bible-verse-include-verse-numbers nil
  "Set to t if quotations should include verse numbers, nil by default."
  :group 'bible-verse
  :type 'boolean)


(defvar bible-verse--books-list
  '("Genesis" "Exodus" "Leviticus" "Numbers" "Deuteronomy" "Joshua" "Judges"
    "Ruth" "1Samuel" "2Samuel" "1Kings" "2Kings" "1Chronicles" "2Chronicles"
    "Ezra" "Nehemiah" "Esther" "Job" "Psalms" "Proverbs" "Ecclesiastes"
    "Song of Solomon" "Isaiah" "Jeremiah" "Lamentations" "Ezekiel" "Daniel"
    "Hosea" "Joel" "Amos" "Obadiah" "Jonah" "Micah" "Nahum" "Habakkuk"
    "Zephaniah" "Haggai" "Zechariah" "Malachi" "Matthew" "Mark" "Luke" "John"
    "Acts" "Romans" "1Corinthians" "2Corinthians" "Galatians" "Ephesians"
    "Philippians" "Colossians" "1Thessalonians" "2Thessalonians" "1Timothy"
    "2Timothy" "Titus" "Philemon" "Hebrews" "James" "1Peter" "2Peter" "1John"
    "2John" "3John" "Jude" "Revelation")
  "List of all the books of the Christian bible.")


(defun bible-verse--format-url (book chap vStart vEnd)
  "Use BOOK, CHAP, and [VSTART, VEND] to create a url to retrieve the quote.
Value depends on `bible-verse-source` and `bible-verse-translation`.
Return value is a url object (from the URL package)."
  (pcase bible-verse-source
    ("http://labs.bible.org/api/?"
     (concat
      bible-verse-source
      "formatting=plain"
      "&passage="
      book
      "+"
      (number-to-string chap)
      ":"
      (number-to-string vStart)
      "-"
      (number-to-string vEnd)))))


(defun bible-verse--retrieve-quote (book chap vStart vEnd)
  "Ask user for BOOK, CHAP, and verse range, then insert relevant link & quote.
Verse range = [VSTART, VEND]."
  (let '(queryUrl (bible-verse--format-url book chap vStart vEnd))
    (with-temp-buffer
      (url-insert-file-contents queryUrl)
      (while (re-search-forward (rx (1+ space) (group (1+ digit)) (1+ space)) nil t)
        (if bible-verse-include-verse-numbers
            (replace-match " [\\1] " nil nil)
            (replace-match " " nil nil)))
      (delete-trailing-whitespace)
      (goto-char (point-min))
      (re-search-forward "[0-9]+:[0-9]+ ")
      (buffer-substring (point) (point-max)))))


(defun bible-verse--make-link (book chap)
  "BOOK CHAP VSTART VEND."
  (concat
   "https://netbible.org/bible/"
   book
   "+"
   (number-to-string chap)))



(defun bible-verse--insert-snippet (book chap verse &optional verse-end)
  "BOOK CHAP VERSE VERSE-END."
  (let
      ;; Get the url for the link, and retrieve the quotation, with the last
      ;; parameter being dependent on VERSE-END.
      ((link (bible-verse--make-link book chap))
       (quotation (bible-verse--retrieve-quote book chap verse (if verse-end verse-end verse))))

    ;; Begin insertion of the actual link and quote into the buffer.
    (insert
     ;; Ensure the link is not inserted if the user says so.
     (unless (equal bible-verse-contents "quote")
       (concat "\n[[" link "]["
               book " "
               (number-to-string chap) ":"
               (number-to-string verse)
               (when verse-end (concat "-" (number-to-string verse-end)))
               "]]"))

     ;; Likewise only insert the quotation if required by the user.
     (unless (equal bible-verse-contents "link")
       (concat "\n#+begin_quote\n"
               quotation
               "\n#+end_quote\n")))))


(defun bible-verse--snippet-prompt (quoting-many)
  "Ask the user for what portion of the bible to get.
If QUOTING-MANY is nil, ask for and return only a single verse.
Otherwise, ask for and return a range of verses."
  (bible-verse--insert-snippet
   (completing-read "Book of the bible to quote: " bible-verse--books-list)
   ;; (read-number (concat "Chapter (less than " (number-to-string 150) "): "))
   ;; (read-number (concat "Verse (less than " (number-to-string 176) "): "))
   ;; (when quoting-many
   ;;   (read-number (concat "Last verse (less than " (number-to-string 176) "): ")))))
   (read-number "Chapter: ")
   (read-number "Verse: ")
   (when quoting-many
     (read-number "Last verse: "))))


(defun bible-verse-single ()
  "Ask for bible book, chapter, and verse, then insert relevant link & quote."
  (interactive)
  (bible-verse--snippet-prompt nil))


(defun bible-verse-many ()
  "Ask for bible book, chapter, and verse range, then insert relevant link & quote."
  (interactive)
  (bible-verse--snippet-prompt t))


;;;###autoload
(define-minor-mode bible-verse-mode
  "Insert quotations from the bible into org buffers, interactively."
  :init-value nil
  :lighter " BibleV"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "C-'") 'bible-verse-single)
            (define-key map (kbd "C-\"") 'bible-verse-many)
            map)
  :group 'bible-verse
  :global nil)


(provide 'bible-verse-mode)
;;; bible-verse-mode.el ends here
